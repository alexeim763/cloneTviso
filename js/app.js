$(document).ready(function () {
    //Init Javascript

    //Function slideNotification
    $('.Notification').on('click', function (ev) {
        ev.preventDefault();
        $('.NotificationSlide').toggle();
    });
    $('#SlidePeoples-content').mCustomScrollbar({
        theme: "rounded",
        axis: 'y',
        scrollButtons: {
            enable: true
        }
    });
});
